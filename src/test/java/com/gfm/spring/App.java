package com.gfm.spring;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

	public static void main(String[] args) {
		App a = new App();
		a.init();
	}

	private ClassPathXmlApplicationContext context;

	public void init() {

		String home = "WEB-INF/spring/";
		context = new ClassPathXmlApplicationContext(new String[] {
				home + "persistence-context.xml",
				home + "/servlet/servlet-context.xml" });

	}

}