package com.gfm.spring.schedule;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class FooSchedule {
	
	private static Logger log = Logger.getLogger(FooSchedule.class);

    @Scheduled(fixedDelay=10000)/*10  sec */
    public void sayHello(){
		log.info("Hello! ");
	}
    
    @Scheduled(cron="0/30 * * * * ?")
    public void cron_30sec(){
    	log.info("cron_30sec es: 00:00:00 / 00:00:30 / 00:01:00! ");
    }
    
    @Scheduled(cron="0 0/30 * * * ?")
    public void cron_30min(){
    	log.info("cron_30min es: 00:00:00 / 00:30:00 / 01:00:00! ");
    }
    
    @Scheduled(cron="0 0 0/1 * * ?")
    public void cron_hourly(){
    	log.info("cron_hourly es: 00:00:00 / 01:00:00 / 02:00:00! ");
    }
    
    /* print each day at 18:28 */
    @Scheduled(cron="0 0/28 0/18 * * ?")
    public void cron_fixed_hour(){
    	log.info("cron_fixed_hour");
    }
    
    
}
