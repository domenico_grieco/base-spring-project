package com.gfm.spring;

public class FooResponse {

	
	public final String message;
	public final String[] params;
	
	public FooResponse(String message, String[] params) {
		super();
		this.message = message;
		this.params = params;
	}

}
