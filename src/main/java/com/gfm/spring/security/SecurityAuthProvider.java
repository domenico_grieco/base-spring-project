package com.gfm.spring.security;

import java.util.List;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
 
@Component
public class SecurityAuthProvider implements AuthenticationProvider 
{

	
	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		  String name = authentication.getName();
	      String password = authentication.getCredentials().toString();
	 
	      // use the credentials to try to authenticate against the third party system
	      if (authenticatedAgainstThirdPartySystem(name,password)) {
	            List<GrantedAuthority> grantedAuths = Lists.newArrayList();
	            return new UsernamePasswordAuthenticationToken(name, password, grantedAuths);
	      } else {
	            throw new BadCredentialsException("Unable to auth against third party systems");	             
	      }
	}

	private boolean authenticatedAgainstThirdPartySystem(String name, String password) throws AuthenticationException{		
		return name.equals(password);
	}

	@Override
	public boolean supports(Class<?> auth) {
		return auth.equals(UsernamePasswordAuthenticationToken.class);
	}
	
}