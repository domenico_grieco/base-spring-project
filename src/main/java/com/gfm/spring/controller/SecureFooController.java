package com.gfm.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gfm.spring.FooResponse;
import com.gfm.spring.service.FooService;

@Controller
@RequestMapping("/secure")
public class SecureFooController {
	
	@Autowired
	FooService service;
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String home(Authentication authentication,Model model){
		model.addAttribute("username", authentication.getName());
		return "home";
	}
	
	
	@RequestMapping(value="/hello/{username}", method=RequestMethod.GET)
	public @ResponseBody FooResponse hello(
			@PathVariable String username,
			@RequestParam(defaultValue="true") boolean sync){
		if(sync){
			service.asyncMethod(username);
			return new FooResponse("success", new String[]{"secure","waiting for result!"});
		}
		
		return new FooResponse("success", new String[]{"secure",service.syncMethod(username)});
	}
	
}
