package com.gfm.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gfm.spring.FooResponse;
import com.gfm.spring.service.FooService;

@Controller
@RequestMapping("/")
public class FooController {
	
	@Autowired
	FooService service;
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String login(Authentication authentication,Model model){
		if(authentication == null || (authentication!=null && !authentication.isAuthenticated()))
			model.addAttribute("error","ops... ");
		return "login";
	}
	
	@RequestMapping(value="/hello/{username}", method=RequestMethod.GET)
	public @ResponseBody FooResponse hello(
			@PathVariable String username,
			@RequestParam(defaultValue="true") boolean sync){
		if(sync){
			service.asyncMethod(username);
			return new FooResponse("success", new String[]{"waiting for result!"});
		}
	
		return new FooResponse("success", new String[]{service.syncMethod(username)});
	}
	
}


