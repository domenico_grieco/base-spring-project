package com.gfm.spring.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;




@Service
public class FooService {

	private static Logger log = Logger.getLogger(FooService.class);

	@Async
	public void asyncMethod(String msg)
	{
		log.info("sleep for 10 sec");
		try {
			Thread.sleep(10000l);
		} catch (InterruptedException e) {
			log.error(e.getMessage(),e);
		}
		log.info("Result : "+StringUtils.reverse(msg));
		log.info("async method terminated");
	}

	
	public String syncMethod(String msg){
		return StringUtils.reverse(msg);
	}

}