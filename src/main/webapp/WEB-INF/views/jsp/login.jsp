<?xml version="1.0" encoding="UTF-8"?>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 
 <body>	
		
	<div id="login-box">
	<center>
 		<div style="width:350px">
 
		<h3><spring:message code="signin"/></h3>
 
		<c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
 		
		<form name="loginForm" class="form-horizontal"  action="<c:url value='/j_spring_security_check' />" method="post">
						
		<div class="form-group">
			  <input type="text" class="form-control" id="j_username" name="j_username" placeholder="<spring:message code="username"/>" required/>
		</div>
		
		<div class="form-group">
			  <input type="password" class="form-control" id="j_password" name="j_password" placeholder="<spring:message code="password"/>" required/>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-default"><spring:message code="signin"/></button>
		</div>   
				   

		</form>
		</div>
		</center>
	
	</div>
		
</body>
 
 

</html>
