all:
	mvn clean install -DskipTests

jar:
	mvn package -DskipTests

idea:
	mvn idea:idea

eclipse:
	mvn eclipse:eclipse
	
install:
	mvn install -DskipTests
