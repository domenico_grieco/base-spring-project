base-spring-project
===============

Base Spring Project

this project contains example for:
- REST api
- Async methods
- Authenication
- Scheduled methods

Tomcat
===============
1. download Tomcat binary from the official website
2. extract archive (suggested folder for tomcat in our setting is /opt/apache/tomcat/<tomcat\_version> with the soft link _latest_)
3. under /bin there are startup.sh and shutdown.sh scripts to start/stop tomcat


for Tomcat 8 using our setting

```
mkdir -p /opt/apache/tomcat

cd /opt/apache/tomcat

wget http://it.apache.contactlab.it/tomcat/tomcat-8/v8.0.21/bin/apache-tomcat-8.0.21.tar.gz

tar xvf apache-tomcat-8.0.21.tar.gz

rm -rf apache-tomcat-8.0.21.tar.gz

ln -s apache-tomcat-8.0.21 latest
```

How to use Tomcat in Eclipse J2EE
=================================
1. File>New>Other...>Server
2. select tomcat home ( in our setting /opt/apache/tomcat/latest)
3. apply
 
To start/stop use the _Servers_ tab


Deploy and test in Eclipse J2EE
=================================
To deployment you can use directly Eclipse:
1. _Servers tab_ click on Tomcat and then _Add or Remove_
2. run the Tomcat server with _run_ icon
 
if the deployment has worked, you may read scheduled log messages in console 
1. a message each 10 sec
2. a message each 30 sec
3. a message at specified time

To the API REST (simply response with reverse string) you can use the following links 
where *APP-NAME* is the name that you have choiced for the app. The default app-name given
is tipically the project name. If you deploy with Eclipse, you may known this name in _Servers tab_:
1. right button on Tomcat 
2. _open_
3. select in the bottom _modules_
4. the name is in the  _path_ column in the table 

*Sync REST GET*:
http://localhost:8080/APP-NAME/hello/YOUR-STRING/?sync=true
http://localhost:8080/APP-NAME/hello/YOUR-STRING/

*ASync REST GET* (after 10 sec print in console):
http://localhost:8080/< project-name >/hello/YOUR-STRING/?sync=false

with *authentication* (use same word for username and password ex. pippo/pippo)
http://localhost:8080/secure/APP-NAME/hello/YOUR-STRING/?sync=true
http://localhost:8080/secure/APP-NAME/hello/YOUR-STRING/
http://localhost:8080/secure/APP-NAME/hello/YOUR-STRING/?sync=false


How to use this project as Seed for another project
==================================================
This project may be used as base project. 
This is the suggested procedure:

```
1. clone the project

2. make YOUR-PROJECT-NAME

3. cp base-spring-project/* YOUR-PROJECT-NAME

4. cd YOUR-PROJECT-NAME

5. rm -rf .git
```

6. edit *groupId*, *artifactId*, *name* in  _pom.xml_

```
7. make eclipse (or idea)
```

8. import project in your ide

